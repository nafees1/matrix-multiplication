
#include <iostream>
#include <vector>

#include <pthread.h>
#include <sys/time.h>
#include "vector.h"
#include "matrix.h"
#define no_of_thread 4
using namespace std;
int main()
{
	
pthread_t* thread;

double elap;    
clock_t start;
clock_t stop;
	
//printf("no of thread %d",no_of_thread);
//printf("Matrix  with Vector Multiplication");
	
start=clock();
	for (int i = 0; i < no_of_thread; i++)
	{
	  
	  if (pthread_create (&thread[i], NULL, vec_mul, (void*)i) != 0 )
	  {
	   // perror("Can't create thread");
	
	  }
	}


	for (int i = 0; i < no_of_thread; i++)
	  pthread_join (thread[i], NULL);
	
	stop=clock();
	elap=double (stop-start)/CLOCKS_PER_SEC;
	
	
	cout<<"Elapsed time: "<<elap<<endl;

	

	pthread_exit(NULL);
	
/////////////////////////

//printf("Matrix  with matrix Multiplication");
	
start=clock();
	for (int i = 0; i < no_of_thread; i++)
	{
	  // creates each thread working on its own slice of i
	  if (pthread_create (&thread[i], NULL, mat_mul, (void*)i) != 0 )
	  {
	  //  perror("Can't create thread");
	  
	  }
	}

	
	for (int i = 0; i < no_of_thread; i++)
	  pthread_join (thread[i], NULL);
	stop=clock();
	elap=double (stop-start)/CLOCKS_PER_SEC;
	
	
	//printf("Elapsed time: %d",elap);


	pthread_exit(NULL);


	return 0;
}
