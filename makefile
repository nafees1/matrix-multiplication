LIBS ?= -lpthread
OBJS = main.o matrix.o vector.o

prog1 : $(OBJS)
	g++ -o prog $(OBJS) -pthread 
matrix.o : matrix.cc matrix.h
	g++ -c matrix.cc
vector.o : vector.cc vector.h
	g++ -c vector.cc
main.o : main.cc vector.h matrix.h
	g++ -c main.cc 
clean :
	rm $(OBJS)
